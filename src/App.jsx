import { BrowserRouter } from 'react-router-dom';
import Grid from '@mui/material/Grid';
import HeaderRoutes from './core/Routes/HeaderRoutes';
import AsideRoutes from './core/Routes/AsideRoutes';
import FooterRoutes from './core/Routes/FooterRoutes';
import PagesRoutes from './core/Routes/PagesRoutes';

import initServices from './core/services/initServices';

import './App.css';

function App() {
  initServices.init(); //initialize services
  return (
    <>
      <BrowserRouter>
        <Grid container direction='column' style={{ minHeight: '100vh' }}>
          <Grid item>
            {/* Header */}
            <HeaderRoutes />
          </Grid>

          <Grid item container>
            {/* Aside */}
            <Grid item xs={12} md={2}>
              <AsideRoutes />
            </Grid>

            {/* Main Content */}
            <Grid item xs={12} md={10}>
              <PagesRoutes />
            </Grid>
          </Grid>

          <Grid item>
            {/* Footer */}
            <FooterRoutes />
          </Grid>
        </Grid>
      </BrowserRouter>
    </>
  );
}

export default App;
