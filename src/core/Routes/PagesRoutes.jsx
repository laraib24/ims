import React, { Suspense } from 'react';
import { Navigate, useRoutes, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
const Home = React.lazy(() => import('../../views/pages/Home/Home'));
const LoginForm = React.lazy(() => import('../../views/pages/Auth/Login'));

import CircularProgress from '@mui/material/CircularProgress';

const PagesRoutes = () => {
  const routes = [
    {
      path: '/',
      element: (
        <Suspense fallback={<CircularProgress />}>
          <Home />
        </Suspense>
      )
    },
    {
      path: '/login',
      element: (
        <AuthRoute>
          <Suspense fallback={<CircularProgress />}>
            <LoginForm />
          </Suspense>
        </AuthRoute>
      )
    },

    {
      path: '*',
      element: (
        <div>
          <div
            className='w-100 d-flex justify-content-center align-items-center flex-column'
            style={{ height: '500px' }}
          >
            <h1>404</h1>
            <h2>OOPS!</h2>
            <h3>Page Not Found</h3>
          </div>
        </div>
      )
    }
  ];

  const router = useRoutes(routes);

  return router;
};

export default PagesRoutes;
export function ProtectedRoute({ children }) {
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
  if (!isAuthenticated) {
    const location = useLocation();
    const redirectURL = location.pathname;
    console.print('auth: ', redirectURL);

    window.localStorage.setItem('redirectURL', redirectURL);
  }
  return isAuthenticated ? children : <Navigate to='/login' replace />;
}

export function AuthRoute({ children }) {
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
  let redirectRoute = '/';

  if (isAuthenticated) {
    const redirectURL = localStorage.getItem('redirectURL');
    localStorage.removeItem('redirectURL');
    if (redirectURL) redirectRoute = redirectURL;
  }

  return isAuthenticated ? (
    <Navigate to={`${redirectRoute}?firstLogin=true`} replace />
  ) : (
    children
  );
}
