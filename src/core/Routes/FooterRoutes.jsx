import React from "react";
import { useRoutes } from "react-router-dom";

import MainFooter from "../../views/Components/Footers/MainFooter";

const FooterRoutes = () => {
  const routes = [
    // {
    //   path: "/login",
    //   element: <></>,
    // },
    {
      path: "*",
      element: <MainFooter />,
    },
  ];

  const router = useRoutes(routes);

  return router;
};

export default FooterRoutes;
