import React from 'react';
import { useRoutes } from 'react-router-dom';

import MainAside from '../../views/Components/Asides/MainAside';

const AsideRoutes = () => {
  const routes = [
    // {
    //   path: '/login',
    //   element: <></>
    // },
    {
      path: '*',
      element: <MainAside />
    }
  ];

  const router = useRoutes(routes);

  return router;
};

export default AsideRoutes;
