import React from "react";
import { useRoutes } from "react-router-dom";

import MainHeader from "../../views/Components/Headers/MainHeader";

const HeaderRoutes = () => {
  const routes = [
    // {
    //   path: "/login",
    //   element: <></>,
    // },
    {
      path: "*",
      element: <MainHeader />,
    },
  ];

  const router = useRoutes(routes);

  return router;
};

export default HeaderRoutes;
